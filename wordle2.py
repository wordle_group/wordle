import requests as rq
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = MMBot.words
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = str(rj["feedback"])
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        while not won:
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
            
            if len(tries) > 6:
                return("Great try. Do again to win the game")
            else :
                return ("Secret is", choice, "found in", len(tries), "attempts") + ("Route is:", " => ".join(tries))
                


    def update(self, choice: str, output: str):
        for i in range(5):
            if "G" == output[i]:
                self.choices = [w for w in self.choices if choice[i] == w[i] ]
            if "Y" == output[i]:
                self.choices = [w for w in self.choices if choice[i] in w ]
            if "R" == output[i]:
                self.choices = [w for w in self.choices if choice[i] not in w ]
            
game = MMBot("Strugglers")
print(game.play())

