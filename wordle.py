import requests as rq
import random
DEBUG = False

class MMBot:
    with open("words.txt") as f:
        words = [word.strip() for word in f]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):

        self.session = rq.session()
<<<<<<< HEAD
        reigister_dict = {"mode": "wordle", "name": name}
=======
        register_dict = {"mode": "wordle", "name": bot}
>>>>>>> 73a6c77ab6cb4f771d8a43162d5602d56ff4bbff
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = MMBot.words
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = (rj["feedback"])
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        while not won:
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, word):
        self.choice = [i for i in choice]
	    wordlist = [i for i in word]
	    message = []
	    for choice, actual in zip(self.choice, wordlist):
		    if choice == actual:
			   message.append('G')
		    elif choice in word:
			   message.append('Y')
		    else:
			   message.append('R')
	    return ''.join(i for i in message)
            
     


game = MMBot("CodeShifu")
game.play()


